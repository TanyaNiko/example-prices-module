import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-price-approve-dialog',
  templateUrl: './price-approve-dialog.component.html',
  styleUrls: ['./price-approve-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PriceApproveDialogComponent implements OnInit {
  static defaultConf = {
    width: '282px'
  };

  constructor(
    private dialogRef: MatDialogRef<PriceApproveDialogComponent>
  ) { }

  public save() {
    this.dialogRef.close({ saved: true });
  }

  public ngOnInit(): void {}
}
