import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialogModule, MatDialogRef } from '@angular/material/dialog';

import { PriceApproveDialogComponent } from './price-approve-dialog.component';

describe('PriceApproveDialogComponent', () => {
  let component: PriceApproveDialogComponent;
  let fixture: ComponentFixture<PriceApproveDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [MatDialogModule],
      declarations: [PriceApproveDialogComponent],
      providers: [
        {
          provide: MatDialogRef,
          useValue: {
            width: '282px'
          }
        }
      ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PriceApproveDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
