import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { NgxPermissionsModule } from 'ngx-permissions';
import { ToastrModule } from 'ngx-toastr';
import { DateFactory } from 'src/app/core/classes/date/date-factory';
import { DateFormatter } from 'src/app/core/classes/date/date-formatter';
import { DATE_TIMEZONE } from 'src/app/core/services/date/date.tokens';
import { DateFactoryService } from 'src/app/core/services/date/date-factory.service';
import { SearchService } from 'src/app/core/services/search/search.service';
import { SharedModule } from 'src/app/shared/shared.module';

import { FiltersComponent } from '../../filters/filters.component';
import { PricesTableComponent } from '../../prices-table/prices-table.component';
import { TooltipPriceComponent } from '../../tooltip-price/tooltip-price.component';
import { PricesEditComponent } from './prices-edit.component';

describe('PricesEditComponent', () => {
  let component: PricesEditComponent;
  let fixture: ComponentFixture<PricesEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        RouterTestingModule.withRoutes([]),
        ToastrModule.forRoot(),
        NgxPermissionsModule.forRoot(),
        SharedModule,
        BrowserAnimationsModule
      ],
      declarations: [
        PricesEditComponent,
        PricesTableComponent,
        TooltipPriceComponent,
        FiltersComponent
      ],
      providers: [
        SearchService,
        DateFormatter,
        FormBuilder,
        {
          provide: DateFactory,
          useExisting: DateFactoryService
        },
        {
          provide: DATE_TIMEZONE,
          useValue: 'UTC+3'
        }
      ]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PricesEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
