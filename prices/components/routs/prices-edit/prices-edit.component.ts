import { ChangeDetectionStrategy, Component, OnInit, ViewChild } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { CanChildFormComponentDeactivate } from '../../../../../shared/guards/interfaces';
import { SelectedPriceTypeService } from '../../../services/selected-price-type.service';
import { PricesTableComponent } from '../../prices-table/prices-table.component';

@Component({
  selector: 'app-prices-edit',
  templateUrl: './prices-edit.component.html',
  styleUrls: ['./prices-edit.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PricesEditComponent
implements OnInit, CanChildFormComponentDeactivate {
  @ViewChild(PricesTableComponent) childFormComponent!: PricesTableComponent;

  readonly positionPromoPrice = 5;

  readonly positionPromoPriceNew = 7;

  columns = [
    'productSap',
    'materialSupplierCode',
    'manufacturingCode',
    'productName',
    'price',
    'priceNew',
    'dateEdit',
    'retailerNameAndStatus'
  ];

  displayedColumnsEdit$ = new Observable<string[]>();

  constructor(private selectedPriceTypeService: SelectedPriceTypeService) {
    this.displayedColumnsEdit$ = this.getDynamicColumns();
  }

  ngOnInit(): void {}

  private getDynamicColumns(): Observable<string[]> {

    return this.selectedPriceTypeService.showPromoPrices$.pipe(
      map((showPromoPrices) => {
        if (showPromoPrices) {
          this.columns.splice(this.positionPromoPrice, 0, 'promoPrice');
          this.columns.splice(this.positionPromoPriceNew, 0, 'promoPriceNew');
        } else {
          const indexPromoPrice = this.columns.findIndex(
            (name) => name === 'promoPrice'
          );
          if (indexPromoPrice > -1) {
            this.columns.splice(indexPromoPrice, 1);
          }
          const indexPromoPriceNew = this.columns.findIndex(
            (name) => name === 'promoPriceNew'
          );
          if (indexPromoPriceNew > -1) {
            this.columns.splice(indexPromoPriceNew, 1);
          }
        }
        return this.columns;
      })
    );
  }
}
