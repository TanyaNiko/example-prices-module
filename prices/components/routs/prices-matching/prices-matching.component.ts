import {
  ChangeDetectionStrategy,
  Component
} from '@angular/core';

@Component({
  selector: 'app-prices-matching',
  templateUrl: './prices-matching.component.html',
  styleUrls: ['./prices-matching.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PricesMatchingComponent {

  title = 'Согласование цен';

  displayedColumns = [
    'productSap',
    'materialSupplierCode',
    'manufacturingCode',
    'productName',
    'retailerNameAndStatus',
    'newOldPrice',
    'percentageChangePrice',
    'dateOutNew',
    'approve'
  ];

  constructor(){}

}
