import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { NgxPermissionsModule } from 'ngx-permissions';
import { ToastrModule } from 'ngx-toastr';
import { DateFactory } from 'src/app/core/classes/date/date-factory';
import { DateFormatter } from 'src/app/core/classes/date/date-formatter';
import { DATE_TIMEZONE } from 'src/app/core/services/date/date.tokens';
import { DateFactoryService } from 'src/app/core/services/date/date-factory.service';
import { SearchService } from 'src/app/core/services/search/search.service';
import { SharedModule } from 'src/app/shared/shared.module';

import { FiltersComponent } from '../../filters/filters.component';
import { PricesTableComponent } from '../../prices-table/prices-table.component';
import { SelectCompanyComponent } from '../../select-company/select-company.component';
import { TooltipPriceComponent } from '../../tooltip-price/tooltip-price.component';
import { PricesMatchingComponent } from './prices-matching.component';

describe('PricesMatchingComponent', () => {
  let component: PricesMatchingComponent;
  let fixture: ComponentFixture<PricesMatchingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        SharedModule,
        BrowserAnimationsModule,
        RouterTestingModule.withRoutes([]),
        NgxPermissionsModule.forRoot(),
        ToastrModule.forRoot()
      ],
      declarations: [
        PricesMatchingComponent,
        FiltersComponent,
        PricesTableComponent,
        TooltipPriceComponent,
        SelectCompanyComponent
      ],
      providers: [
        SearchService,
        DateFormatter,
        {
          provide: DateFactory,
          useExisting: DateFactoryService
        },
        {
          provide: DATE_TIMEZONE,
          useValue: 'UTC+3'
        }
      ]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PricesMatchingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
