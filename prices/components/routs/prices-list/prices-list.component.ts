import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { SelectedPriceTypeService } from '../../../services/selected-price-type.service';

@Component({
  selector: 'app-prices-list',
  templateUrl: './prices-list.component.html',
  styleUrls: ['./prices-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PricesListComponent implements OnInit {
  readonly positionPromoPrice = 5;

  columns = [
    'productSap',
    'materialSupplierCode',
    'manufacturingCode',
    'productName',
    'price',
    'priceStatus',
    'priceERPStatus',
    'dateOut',
    'retailerNameAndStatus'
  ];

  displayedColumnsList$ = new Observable<string[]>();

  constructor(private selectedPriceTypeService: SelectedPriceTypeService) {
    this.displayedColumnsList$ = this.getDynamicColumns();
  }

  ngOnInit(): void {}

  private getDynamicColumns(): Observable<string[]> {
    return this.selectedPriceTypeService.showPromoPrices$.pipe(
      map((showPromoPrices) => {
        if (showPromoPrices) {
          this.columns.splice(this.positionPromoPrice, 0, 'promoPrice');
        } else {
          const index = this.columns.findIndex((name) => name === 'promoPrice');
          if (index > -1) {
            this.columns.splice(index, 1);
          }
        }
        return this.columns;
      })
    );
  }
}
