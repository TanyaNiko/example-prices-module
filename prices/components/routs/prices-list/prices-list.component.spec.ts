import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { NgxPermissionsModule } from 'ngx-permissions';
import { ToastrModule } from 'ngx-toastr';
import { DateFactory } from 'src/app/core/classes/date/date-factory';
import { DateFormatter } from 'src/app/core/classes/date/date-formatter';
import { DATE_TIMEZONE } from 'src/app/core/services/date/date.tokens';
import { DateFactoryService } from 'src/app/core/services/date/date-factory.service';
import { SearchService } from 'src/app/core/services/search/search.service';
import { SharedModule } from 'src/app/shared/shared.module';

import { FiltersComponent } from '../../filters/filters.component';
import { PricesTableComponent } from '../../prices-table/prices-table.component';
import { TooltipPriceComponent } from '../../tooltip-price/tooltip-price.component';
import { PricesListComponent } from './prices-list.component';

describe('PricesListComponent', () => {
  let component: PricesListComponent;
  let fixture: ComponentFixture<PricesListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        NgxPermissionsModule.forRoot(),
        ToastrModule.forRoot(),
        RouterTestingModule.withRoutes([]),
        HttpClientTestingModule,
        SharedModule,
        BrowserAnimationsModule
      ],
      declarations: [
        PricesListComponent,
        PricesTableComponent,
        TooltipPriceComponent,
        FiltersComponent
      ],
      providers: [
        SearchService,
        FormBuilder,
        DateFormatter,
        {
          provide: DateFactory,
          useExisting: DateFactoryService
        },
        {
          provide: DATE_TIMEZONE,
          useValue: 'UTC+3'
        }
      ]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PricesListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
