import { ChangeDetectionStrategy, Component, EventEmitter, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import {
  MAT_FORM_FIELD_DEFAULT_OPTIONS,
  MatFormFieldDefaultOptions
} from '@angular/material/form-field';

@Component({
  selector: 'app-filters',
  templateUrl: './filters.component.html',
  styleUrls: ['./filters.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: MAT_FORM_FIELD_DEFAULT_OPTIONS,
      useValue: {
        appearance: 'legacy'
      } as MatFormFieldDefaultOptions
    }
  ]
})
export class FiltersComponent {
  @Output() resetFilters = new EventEmitter<any>();

  public readonly form: FormGroup;

  constructor(private fb: FormBuilder) {
    this.form = this.buildForm();
  }

  public handleResetFilters(): void {
    this.resetFilters.emit();
    this.form.reset();
  }

  private buildForm(): FormGroup {
    return this.fb.group({
      groupCode: [null],
      brandCode: [null]
    });
  }
}
