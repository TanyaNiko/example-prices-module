import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SearchService } from 'src/app/core/services/search/search.service';
import { RegistrationService } from 'src/app/pages/registration-org/services/registration.service';
import { SelectSearchModule } from 'src/app/shared/components/select-search/select-search.module';

import { FiltersComponent } from './filters.component';

describe('FiltersComponent', () => {
  let component: FiltersComponent;
  let fixture: ComponentFixture<FiltersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule,
        SelectSearchModule,
        BrowserAnimationsModule
      ],
      declarations: [FiltersComponent],
      providers: [
        SearchService,
        RegistrationService,
        FormBuilder
      ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FiltersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
