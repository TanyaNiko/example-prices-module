import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PricesModule } from '../../prices.module';
import { PriceStatusImgComponent } from './price-status-img.component';

describe('PriceStatusImgComponent', () => {
  let component: PriceStatusImgComponent;
  let fixture: ComponentFixture<PriceStatusImgComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [PricesModule],
      declarations: [PriceStatusImgComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PriceStatusImgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
