import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';

import { PriceStatus } from '../../../products/enums/price-status';
import { PriceSapErp } from '../../../products/enums/price-status-sap-erp';

@Component({
  selector: 'app-price-status-img',
  templateUrl: './price-status-img.component.html',
  styleUrls: ['./price-status-img.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PriceStatusImgComponent implements OnInit {
  @Input() status: PriceStatus | PriceSapErp;

  constructor() {
    this.status = PriceStatus.invalid;
  }

  ngOnInit(): void {}
}
