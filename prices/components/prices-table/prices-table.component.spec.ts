import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { NgxPermissionsModule } from 'ngx-permissions';
import { ToastrModule } from 'ngx-toastr';
import { DateFactory } from 'src/app/core/classes/date/date-factory';
import { DateFormatter } from 'src/app/core/classes/date/date-formatter';
import { DATE_TIMEZONE } from 'src/app/core/services/date/date.tokens';
import { DateFactoryService } from 'src/app/core/services/date/date-factory.service';
import { SearchService } from 'src/app/core/services/search/search.service';
import { RegistrationService } from 'src/app/pages/registration-org/services/registration.service';
import { SharedModule } from 'src/app/shared/shared.module';

import { PriceService } from '../../services/price.service';
import { FiltersComponent } from '../filters/filters.component';
import { TooltipPriceComponent } from '../tooltip-price/tooltip-price.component';
import { PricesTableComponent } from './prices-table.component';

describe('PricesTableComponent', () => {
  let component: PricesTableComponent;
  let fixture: ComponentFixture<PricesTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        SharedModule,
        BrowserAnimationsModule,
        RouterTestingModule.withRoutes([]),
        ToastrModule.forRoot(),
        NgxPermissionsModule.forRoot()
      ],
      declarations: [
        PricesTableComponent,
        FiltersComponent,
        TooltipPriceComponent
      ],
      providers: [
        PriceService,
        RegistrationService,
        SearchService,
        DateFormatter,
        {
          provide: DateFactory,
          useExisting: DateFactoryService
        },
        {
          provide: DATE_TIMEZONE,
          useValue: 'UTC+3'
        }
      ]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PricesTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
