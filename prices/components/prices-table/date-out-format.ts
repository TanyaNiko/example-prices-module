import { Platform } from '@angular/cdk/platform';
import { formatDate } from '@angular/common';
import {
  Inject,
  Injectable
} from '@angular/core';
import {
  MAT_DATE_LOCALE,
  NativeDateAdapter
} from '@angular/material/core';

@Injectable()
export class CustomNativeDateAdapter extends NativeDateAdapter {
  constructor(
  @Inject(MAT_DATE_LOCALE)
    matDateLocale: string,
    platform: Platform
  ) {
    super(matDateLocale, platform);
  }

  public format(date: Date, displayFormat: string): string {
    return formatDate(date, displayFormat, this.locale);
  }
}


export const DATE_OUT_FORMAT = {
  parse: {
    dateInput: 'LL'
  },
  display: {
    dateInput: 'dd MMM',
    monthYearLabel: 'YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'YYYY'
  }
};
