import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  Input,
  OnDestroy,
  OnInit,
  ViewChild
} from '@angular/core';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import {
  DateAdapter,
  MAT_DATE_FORMATS
} from '@angular/material/core';
import { MatDialog } from '@angular/material/dialog';
import {
  MAT_FORM_FIELD_DEFAULT_OPTIONS,
  MatFormFieldDefaultOptions
} from '@angular/material/form-field';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { ToastrService } from 'ngx-toastr';
import { BehaviorSubject, combineLatest, merge, Subject } from 'rxjs';
import { filter, map, pluck, startWith, takeUntil, tap } from 'rxjs/operators';
import { CsvErrorsDialogComponent } from 'src/app/shared/components/csv-errors-dialog/csv-errors-dialog.component';

import { DateFactory } from '../../../../core/classes/date/date-factory';
import { Permission } from '../../../../core/enums/permission.enum';
import { PriceType } from '../../../../core/enums/price-type';
import { MediaService } from '../../../../core/services/media/media.service';
import { ResultStatusHandler } from '../../../../shared/classes/result-status-handler';
import { UPLOAD_STATE } from '../../../../shared/components/upload/upload.component';
import { CanComponentDeactivate } from '../../../../shared/guards/interfaces';
import { Utils } from '../../../../shared/helpers/utils';
import {
  ObservableState,
  setState
} from '../../../../shared/operators/set-observable-state.rxjs';
import { PriceCurrency } from '../../../products/enums/price-currency';
import { PriceStatus } from '../../../products/enums/price-status';
import {
  Price,
  PriceApprove,
  PriceHistory
} from '../../../products/interfaces/price.interface';
import { Product } from '../../../products/interfaces/product.interface';
import { PricesDataSource } from '../../mocks/prices-data-source';
import { PriceService } from '../../services/price.service';
import { FiltersComponent } from '../filters/filters.component';
import { PriceApproveDialogComponent } from '../price-aprove-dialog/price-approve-dialog.component';
import {
  CustomNativeDateAdapter,
  DATE_OUT_FORMAT
} from './date-out-format';

@Component({
  selector: 'app-prices-table',
  templateUrl: './prices-table.component.html',
  styleUrls: [
    '../../../../../scss/table/default-table.scss',
    '../../../../../scss/table/table-column.scss',
    './prices-table.component.scss'
  ],
  providers: [
    {
      provide: MediaService,
      useExisting: PriceService
    },
    {
      provide: UPLOAD_STATE,
      useValue: new ObservableState()
    },
    {
      provide: MAT_FORM_FIELD_DEFAULT_OPTIONS,
      useValue: {
        appearance: 'outline'
      } as MatFormFieldDefaultOptions
    },
    {
      provide: DateAdapter,
      useClass: CustomNativeDateAdapter
    },
    {
      provide: MAT_DATE_FORMATS,
      useValue: DATE_OUT_FORMAT
    }
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PricesTableComponent
implements OnInit, OnDestroy, CanComponentDeactivate {
  @Input() displayedColumns: string[];

  @Input() isEditMode = false;

  @Input() isApprovePricesMode = false;

  @Input() title = 'Цены';

  @ViewChild(MatPaginator) paginator!: MatPaginator;

  @ViewChild(MatSort) sort!: MatSort;

  @ViewChild(FiltersComponent, { static: true }) filters!: FiltersComponent;

  pricePrice$: BehaviorSubject<Price[]>;

  historyPrices$: BehaviorSubject<PriceHistory[]>;

  dataSource: PricesDataSource | null;

  pricesForm!: FormArray;

  approvePricesForm!: FormArray;

  readonly permission = Permission;

  readonly minDaysNextStartPrice = 1;

  readonly maxYearsNextStartPrice = 10;

  readonly minDateStartPrice: Date;

  readonly maxDateStartPrice: Date;

  readonly maxLengthPrice = 14;

  readonly filterForm: FormGroup;

  readonly savingState: ObservableState;

  private readonly destroy$: Subject<void>;

  get invalid(): boolean {
    return this.pricesForm?.controls
      .filter((control) => control.value?.promoPrice || control.value?.price)
      .some((control) => control.invalid);
  }

  get hasPrices(): boolean {
    return (
      this.pricesForm?.controls.findIndex(
        (control) => control.value?.promoPrice || control.value?.price
      ) !== -1
    );
  }

  get canSave(): boolean {
    return (
      this.pricesForm &&
      this.hasPrices &&
      this.pricesForm.dirty &&
      !this.invalid
    );
  }

  get pricesValue(): Price[] {
    return this.pricesForm.controls
      .filter(
        (control) =>
          control.dirty && (control.value.price || control.value.promoPrice)
      )
      .map((control) => control.value);
  }

  get invalidApprovePrices(): boolean {
    return this.approvePricesForm?.controls
      .filter((control) => control.dirty)
      .some((control) => control.invalid);
  }

  get approvePricesValue(): PriceApprove[] {
    return this.approvePricesForm.controls
      .filter((control) => control.dirty)
      .map((control) => control.value);
  }

  constructor(
    public priceService: PriceService,
    @Inject(UPLOAD_STATE)
    private uploadState: ObservableState,
    private date: DateFactory,
    private toastr: ToastrService,
    private fb: FormBuilder,
    private cdr: ChangeDetectorRef,
    private readonly dialog: MatDialog
  ) {
    this.destroy$ = new Subject();
    this.dataSource = null;

    this.savingState = new ObservableState();

    this.minDateStartPrice = this.date.today();
    this.minDateStartPrice.setDate(
      this.minDateStartPrice.getDate() + this.minDaysNextStartPrice
    );

    this.maxDateStartPrice = this.date.today();
    this.maxDateStartPrice.setFullYear(
      this.maxDateStartPrice.getFullYear() + this.maxYearsNextStartPrice
    );

    this.filterForm = this.buildFilterForm();

    this.displayedColumns = [];

    this.pricePrice$ = new BehaviorSubject<Price[]>([]);
    this.historyPrices$ = new BehaviorSubject<PriceHistory[]>([]);
  }

  public get displayedColumnFilters(): string[] {
    return this.displayedColumns.map((column) => `${column}Filter`);
  }

  ngOnInit(): void {
    if (!this.isApprovePricesMode) {
      this.init();
    }
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();

    this.dataSource?.disconnect();
  }

  init(supplierCode: string = ''): void {
    this.dataSource = new PricesDataSource(this.priceService, supplierCode);

    this.dataSource?.prices$
      ?.pipe(takeUntil(this.destroy$))
      .subscribe((products) => {
        if (this.isEditMode) {
          this.pricesForm = this.buildPriceForm(products);
        }
        if (this.isApprovePricesMode) {
          this.approvePricesForm = this.buildApprovePriceForm(products);
        }
        this.cdr.detectChanges();
      });

    this.connectFilterer();
    this.connectDataReloader();
    this.connectUploadNotifier();
  }

  setCompany(supplierCode: string = ''): void {
    if (!supplierCode) {
      this.dataSource?.disconnect();
      this.dataSource = null;
      this.notifyNotFoundCompanyError();
      return;
    }
    this.toastr.clear();
    this.init(supplierCode);
  }

  price(productIndex: number): FormGroup {
    return this.pricesForm?.get([productIndex]) as FormGroup;
  }

  save(): void {
    if (this.invalid) {
      return;
    }

    this.priceService
      .update(this.pricesValue)
      .pipe(
        setState(this.savingState),
        tap(() => {
          ResultStatusHandler.handlePost(this.toastr);
          this.pricesForm.markAsPristine();
        })
      )
      .subscribe(
        () => this.notifySaveSuccess(),
        (errors) => {
          const err = errors?.[0];
          if (err) {
            this.notifySaveError(err);
          } else {
            console.error(
              `save priceService ошибка в нестандартном формате: ${errors}`
            );
          }
        }
      );
  }

  isConfirmed(): boolean {
    return !this.pricesForm?.dirty;
  }

  downloadTemplate(): void {
    const filePath = './assets/templates/prices.csv';
    Utils.downloadTemplate(filePath);
  }

  setHoveredRow(row: Product) {
    this.historyPrices$.next(
      row?.currentPrice?.changeHistory || ([] as PriceHistory[])
    );
    this.pricePrice$.next(row?.prices || ([] as Price[]));
  }

  approvePriceForm(productIndex: number): FormGroup {
    return this.approvePricesForm?.get([productIndex]) as FormGroup;
  }

  approvePrice(): void {
    if (this.invalidApprovePrices) {
      return;
    }
    this.openApproveDialog();
  }

  hasError(product: FormGroup): boolean {
    return (
      (product?.controls?.price.value || product?.controls?.promoPrice.value) &&
      !product?.controls?.dateStart?.valid
    );
  }

  private buildPriceForm(products: Product[]): FormArray {
    return this.fb.array(
      products.map(({ productSap, currentPrice }) =>
        this.fb.group({
          currency: PriceCurrency.rub,
          dateStart: [currentPrice?.dateStart],
          materialNumber: productSap,
          price: [null],
          promoPrice: [null],
          priceType: currentPrice?.priceType ?? PriceType.purchase
        })
      )
    );
  }

  private buildApprovePriceForm(products: Product[]): FormArray {
    return this.fb.array(
      products.map((product) =>
        this.fb.group({
          comment: '',
          priceUid: product.prices && product.prices[0]?.uid,
          approved:
            (product.prices && product.prices[0]?.approvalStatus) ||
            PriceStatus.invalid
        })
      )
    );
  }

  private buildFilterForm(): FormGroup {
    return this.fb.group({
      productSap: [null],
      materialSupplierCode: [null],
      manufacturingCode: [null],
      productName: [null],
      price: [null],
      promoPrice: [null]
    });
  }

  private openApproveDialog(): void {
    const dialogRef = this.dialog.open(
      PriceApproveDialogComponent,
      PriceApproveDialogComponent.defaultConf
    );
    dialogRef
      .afterClosed()
      .pipe(
        pluck('saved'),
        filter((saved) => saved)
      )
      .subscribe(() => {
        this.approveSubmit();
      });
  }

  private approveSubmit() {
    this.priceService
      .approveList(this.approvePricesValue)
      .pipe(
        setState(this.savingState),
        tap(ResultStatusHandler.handlePost(this.toastr))
      )
      .subscribe(
        () => {
          this.notifyApproveSuccess();
          this.connectDataReloader();
        },
        () => this.notifyApproveError()
      );
  }

  private connectFilterer(): void {
    combineLatest([
      this.filters?.form.valueChanges.pipe(startWith({})),
      this.filterForm.valueChanges.pipe(startWith({}))
    ])
      .pipe(
        map(([
          primaryFilter,
          _filter
        ]) => ({ ...primaryFilter,
          ..._filter })),
        takeUntil(this.destroy$)
      )
      .subscribe((filters) => this.dataSource?.filter$.next(filters));
  }

  private connectDataReloader(): void {
    merge(this.uploadState.success$, this.savingState.success$)
      .pipe(takeUntil(this.destroy$))
      .subscribe(() => this.dataSource?.reload());
  }

  private connectUploadNotifier(): void {
    this.uploadState.success$
      .pipe(takeUntil(this.destroy$))
      .subscribe(() => this.notifyUploadSuccess());

    this.uploadState.errorSource$
      .pipe(takeUntil(this.destroy$))
      .subscribe((error) => this.notifyUploadError(error));
  }

  private notifySaveError(error: any): void {
    const message = error?.errorDetails
      ? error.errorDetails
      : 'Произошла ошибка при обновлении цен';

    this.toastr.error(message, '', {
      closeButton: true,
      progressBar: true,
      timeOut: 5000
    });
  }

  private notifySaveSuccess(): void {
    this.toastr.success('Цены успешно обновлены', '', {
      closeButton: true,
      progressBar: true,
      timeOut: 5000
    });
  }

  private notifyUploadSuccess(): void {
    this.toastr.success('Файл загружен!', '', {
      closeButton: true,
      progressBar: true,
      timeOut: 5000
    });
  }

  private notifyUploadError(error: any): void {
    if (error.status !== 500) {
      this.dialog.open(
        CsvErrorsDialogComponent,
        CsvErrorsDialogComponent.configFactory({ errors: error })
      );
    } else {
      const message = error?.errorDetails
        ? error.errorDetails
        : 'Не удалось загрузить файл!';

      this.toastr.error(message, '', {
        closeButton: true,
        progressBar: true,
        timeOut: 5000
      });
    }
  }

  private notifyNotFoundCompanyError(): void {
    this.toastr.error(
      'Произошла ошибка загрузки данных по выбранному поставщику: нет supplierCode',
      '',
      {
        closeButton: true,
        progressBar: true,
        timeOut: 5000
      }
    );
  }

  private notifyApproveError(): void {
    this.toastr.error('Произошла ошибка при согласовании цен', '', {
      closeButton: true,
      progressBar: true,
      timeOut: 5000
    });
  }

  private notifyApproveSuccess(): void {
    this.toastr.success('Цены успешно согласованы', '', {
      closeButton: true,
      progressBar: true,
      timeOut: 5000
    });
  }
}
