import { ChangeDetectionStrategy, Component, EventEmitter, Output } from '@angular/core';
import { MAT_FORM_FIELD_DEFAULT_OPTIONS, MatFormFieldDefaultOptions } from '@angular/material/form-field';
import { Observable, of } from 'rxjs';
import { catchError, distinctUntilChanged, shareReplay } from 'rxjs/operators';

import { CompanyPriceFilter } from '../../../../core/interfaces/company.interface';
import { CompanyService } from '../../services/company.service';

@Component({
  selector: 'app-select-company',
  templateUrl: './select-company.component.html',
  styleUrls: ['./select-company.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: MAT_FORM_FIELD_DEFAULT_OPTIONS,
      useValue: {
        appearance: 'legacy'
      } as MatFormFieldDefaultOptions
    }
  ]
})

export class SelectCompanyComponent {
  @Output() setCompany = new EventEmitter<any>();

  readonly companies$: Observable<CompanyPriceFilter[]>;

  constructor(
    private readonly companyService: CompanyService
  ) {
    this.companies$ = this.loadCompanies();
  }

  changeCompany(company: CompanyPriceFilter) {
    this.setCompany.emit(company.supplierCode);
  }

  private loadCompanies(): Observable<CompanyPriceFilter[]> {
    return this.companyService.getCompanies().pipe(
      catchError(this.returnEmpty.bind(this)),
      distinctUntilChanged(),
      shareReplay(1)
    );
  }

  private returnEmpty() {
    return of([]);
  }
}
