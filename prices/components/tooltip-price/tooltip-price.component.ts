import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { Observable } from 'rxjs';

import { PriceStatus } from '../../../products/enums/price-status';
import { PriceSapErp } from '../../../products/enums/price-status-sap-erp';
import { SelectedPriceTypeService } from '../../services/selected-price-type.service';

export interface TooltipPriceItem {
  approvalStatus: PriceStatus;
  dateEnd: string;
  dateStart: string;
  exportStatus: PriceSapErp;
  price: number;
  promoPrice?: number;
}

@Component({
  selector: 'app-price-tooltip-price',
  templateUrl: './tooltip-price.component.html',
  styleUrls: ['./tooltip-price.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TooltipPriceComponent {
  @Input() data: TooltipPriceItem[] = [];

  @Input() hasEndDate?: boolean;

  @Input() hasColor?: boolean;

  @Input() hasStatuses?: boolean;

  maxNumberToDisplay = 5;

  readonly showPromoPrice$: Observable<boolean>;

  constructor(private selectedPriceTypeService: SelectedPriceTypeService) {
    this.hasEndDate = false;
    this.hasColor = false;
    this.showPromoPrice$ = this.selectedPriceTypeService.showPromoPrices$;
  }

  public get containsPromoPrice(): boolean {
    return this.data.filter((elem) => elem.promoPrice).length > 0;
  }

  isPositive(value: number) {
    return value > 0;
  }
}
