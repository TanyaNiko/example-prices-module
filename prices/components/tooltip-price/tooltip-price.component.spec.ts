import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { NgxPermissionsModule } from 'ngx-permissions';
import { RegistrationService } from 'src/app/pages/registration-org/services/registration.service';
import { SharedModule } from 'src/app/shared/shared.module';

import { PriceService } from '../../services/price.service';
import { FiltersComponent } from '../filters/filters.component';
import { TooltipPriceComponent } from './tooltip-price.component';

describe('TooltipPriceComponent', () => {
  let component: TooltipPriceComponent;
  let fixture: ComponentFixture<TooltipPriceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        SharedModule,
        BrowserAnimationsModule,
        RouterTestingModule.withRoutes([]),
        NgxPermissionsModule.forRoot()
      ],
      declarations: [
        TooltipPriceComponent,
        FiltersComponent
      ],
      providers: [
        PriceService,
        RegistrationService
      ]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TooltipPriceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
