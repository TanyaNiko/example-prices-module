import { Pipe, PipeTransform } from '@angular/core';

import { Price } from '../../products/interfaces/price.interface';
interface CompositePrice {
  prices: Price[];
  currentPrice: Price;
}

@Pipe({ name: 'priceIsPositive' })
export class PriceIsPositivePipe implements PipeTransform {
  transform(compositePrice: CompositePrice) {
    const newPrice = compositePrice?.prices?.[0]?.price || 0;
    const oldPrice = compositePrice?.currentPrice?.price || 0;
    return newPrice >= oldPrice;
  }
}
