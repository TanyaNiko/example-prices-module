import { Pipe, PipeTransform } from '@angular/core';

import { PriceStatus } from '../../products/enums/price-status';
import { PriceSapErp } from '../../products/enums/price-status-sap-erp';

@Pipe({ name: 'tooltipStatusName' })
export class TooltipStatusNamePipe implements PipeTransform {
  private static statusNames = new Map<PriceStatus | PriceSapErp, string>([
    [
      PriceStatus.approved,
      'Согласовано'
    ],
    [
      PriceStatus.unApproved,
      'Не согласовано'
    ],
    [
      PriceStatus.invalid,
      'Некорректная'
    ],
    [
      PriceSapErp.nonExportable,
      'Не подлежит выгрузке'
    ],
    [
      PriceSapErp.waitingForApprove,
      'Подлежит согласованию'
    ],
    [
      PriceSapErp.waitingForProcess,
      'Ждет очереди на выгрузку'
    ],
    [
      PriceSapErp.waitingForExport,
      'В очереди на выгрузку'
    ],
    [
      PriceSapErp.expired,
      'Просрочена'
    ],
    [
      PriceSapErp.exported,
      'Выгружена'
    ]
  ]);

  transform(status: PriceStatus | PriceSapErp): string {
    return TooltipStatusNamePipe.statusNames.get(status) ?? '';
  }
}
