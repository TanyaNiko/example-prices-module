import { Pipe, PipeTransform } from '@angular/core';

import { PriceStatus } from '../../products/enums/price-status';
import { PriceSapErp } from '../../products/enums/price-status-sap-erp';

@Pipe({ name: 'priceStatusName' })
export class PriceStatusNamePipe implements PipeTransform {
  transform(statusName: string) {
    switch (statusName) {
      case PriceSapErp.nonExportable:
        return 'non-exportable';
      case PriceSapErp.waitingForApprove:
        return 'waiting-for-approve';
      case PriceSapErp.waitingForProcess:
        return 'waiting-for-process';
      case PriceSapErp.waitingForExport:
        return 'waiting-for-export';
      case PriceSapErp.expired:
        return 'expired';
      case PriceSapErp.exported:
        return 'exported';

      case PriceStatus.approved:
        return 'approved';
      case PriceStatus.unApproved:
        return 'unapproved';
      case PriceStatus.invalid:
      default:
        return 'invalid';
    }
  }
}
