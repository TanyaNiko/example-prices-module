import { Pipe, PipeTransform } from '@angular/core';

import { Price } from '../../products/interfaces/price.interface';
interface CompositePrice {
  prices: Price[];
  currentPrice: Price;
}

@Pipe({ name: 'pricePercentageChanges' })
export class PricePercentageChangesPipe implements PipeTransform {
  transform(compositePrice: CompositePrice) {
    const newPrice = compositePrice?.prices?.[0]?.price || 0;
    const oldPrice = compositePrice?.currentPrice?.price || 0;

    if (!newPrice) {
      return '0%';
    }

    if (!oldPrice) {
      return '+100%';
    }

    const changedPrice = newPrice - oldPrice;
    const prefix = changedPrice > 0 ? '+' : changedPrice < 0 ? '-' : '';

    return `${prefix}${(100 * changedPrice / oldPrice).toFixed(0)}%`;
  }
}
