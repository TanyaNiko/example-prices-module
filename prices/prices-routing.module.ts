import { NgModule } from '@angular/core';
import { Route, RouterModule, Routes } from '@angular/router';
import { NgxPermissionsGuard } from 'ngx-permissions';

import { Permission } from '../../core/enums/permission.enum';
import { CanDeactivateChildFormGuard } from '../../shared/guards/can-deactivate-child-form-guard';
import { PricesComponent } from './components/routs/prices/prices.component';
import { PricesEditComponent } from './components/routs/prices-edit/prices-edit.component';
import { PricesListComponent } from './components/routs/prices-list/prices-list.component';
import { PricesMatchingComponent } from './components/routs/prices-matching/prices-matching.component';

const childes: Route[] = [
  {
    path: '',
    component: PricesListComponent,
    canActivate: [NgxPermissionsGuard],
    data: {
      permissions: {
        only: [
          Permission.ReadRetailPrices,
          Permission.ReadPurchasePrices
        ],
        redirectTo: '../'
      }
    }
  },
  {
    path: 'edit',
    component: PricesEditComponent,
    canDeactivate: [CanDeactivateChildFormGuard],
    canActivate: [NgxPermissionsGuard],
    data: {
      permissions: {
        only: [
          Permission.EditRetailPrices,
          Permission.EditPurchasePrices
        ],
        redirectTo: '../'
      }
    }
  },
  {
    path: 'matching',
    component: PricesMatchingComponent,
    canActivate: [NgxPermissionsGuard],
    data: {
      permissions: {
        only: [
          Permission.ModeratePurchasePrices,
          Permission.ModerateRetailPrices
        ],
        redirectTo: '../'
      }
    }
  }
];

const routes: Routes = [
  {
    path: '',
    component: PricesComponent,
    children: childes
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)]
})
export class PricesRoutingModule {}
