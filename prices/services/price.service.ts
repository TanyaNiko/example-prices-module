import { HttpEvent } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { map, mergeMap, switchMap, take } from 'rxjs/operators';

import { DateFormatter } from '../../../core/classes/date/date-formatter';
import { PriceType, ProductType } from '../../../core/enums/price-type';
import { ApiProductCatalog } from '../../../core/services/api/product-catalog/api-product-catalog.service';
import { HttpParamsService } from '../../../core/services/http-params/http-params.service';
import { MediaService } from '../../../core/services/media/media.service';
import {
  Price,
  PriceApprove,
  PriceUpdate
} from '../../products/interfaces/price.interface';
import { Product } from '../../products/interfaces/product.interface';
import { productMap } from '../../products/services/map/product.map';
import { IPrice } from '../interfaces/prices-list.interface';
import { ApiPriceController } from './api/api-price-controller.service';
import { SelectedPriceTypeService } from './selected-price-type.service';

@Injectable({
  providedIn: 'root'
})
export class PriceService implements MediaService {
  constructor(
    private apiCatalogService: ApiProductCatalog,
    private priceControllerService: ApiPriceController,
    private dateFormatter: DateFormatter,
    private httpParams: HttpParamsService,
    private readonly selectedPriceType: SelectedPriceTypeService
  ) {}

  public get(
    filters: IPrice,
    params: {
      limit: number;
      offset: number;
      sortBy: keyof IPrice;
      sortOrder: 'asc' | 'desc';
      supplierCode?: string;
    }
  ): Observable<{ data: Product[]; totalCount: number }> {
    return this.selectedPriceType.priceType$.pipe(
      switchMap(
        (priceType) =>
          this.apiCatalogService
            .getPrices(
              params.offset / params.limit,
              params.limit,
              priceType,
              params.supplierCode,
              filters,
              params.sortBy,
              params.sortOrder
            )
            .pipe(
              map((res) => {
                if (!res?.data?.content) {
                  return this.emptyResult();
                }

                return {
                  data: res.data.content.map(productMap),
                  totalCount: res.data.totalElements
                };
              })
            )
      )
    );
  }

  emptyResult() {
    return {
      data: [] as Product[],
      totalCount: 0
    };
  }

  public setNewPriceAndDate() {
    return of();
  }

  public update(prices: Price[]): Observable<any> {
    return this.selectedPriceType.priceType$.pipe(
      switchMap((poductType) => {
        const pricesData: PriceUpdate[] = prices.map((price) => ({
          currency: price.currency,
          dateStart: this.dateFormatter.format(price.dateStart),
          materialNumber: price.materialNumber,
          price: +price.price,
          priceType: this.mapProductTypeTopriceType(poductType),
          ...(+price.promoPrice ? { promoPrice: +price.promoPrice } : {}),
          promoPrice: price.promoPrice
        }));
        return this.priceControllerService.postPrices(pricesData);
      }),
      mergeMap((response) =>
        response?.data?.success
          ? of(response)
          : throwError(response?.data?.errors)
      ),
      take(1)
    );
  }

  public approveList(approvedPrices: PriceApprove[]): Observable<any> {
    return this.selectedPriceType.priceType$.pipe(
      switchMap(() =>
        this.priceControllerService.postApproveArray(approvedPrices)
      ),
      take(1)
    );
  }

  mapProductTypeTopriceType(poductType?: ProductType) {
    if (poductType === ProductType.marketplace) {
      return PriceType.retail;
    }
    return PriceType.purchase;
  }

  public upload(file: Blob): Observable<HttpEvent<any>> {
    const formData = new FormData();
    formData.append('file', file);
    return this.priceControllerService
      .postFile(formData)
      .pipe(
        mergeMap((response) =>
          (response as any)?.body?.data?.errors
            ? throwError((response as any).body.data.errors)
            : of(response)
        )
      );
  }
}
