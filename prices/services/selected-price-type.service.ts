import { Injectable } from '@angular/core';
import { NgxPermissionsObject, NgxPermissionsService } from 'ngx-permissions';
import { BehaviorSubject, combineLatest, Observable, of } from 'rxjs';
import { distinctUntilChanged, map, mergeMap, shareReplay } from 'rxjs/operators';

import { Permission } from '../../../core/enums/permission.enum';
import { ProductType } from '../../../core/enums/price-type';

@Injectable({
  providedIn: 'root'
})
export class SelectedPriceTypeService {
  readonly priceType$: Observable<ProductType>;

  readonly visibleToggle$: Observable<boolean>;

  readonly showPromoPrices$: Observable<boolean>;

  private readonly priceTypeChangeable$: Observable<ProductType>;

  private priceTypeChangeable: BehaviorSubject<ProductType>;

  private permissions$: Observable<NgxPermissionsObject>;

  constructor(private permissions: NgxPermissionsService) {
    this.priceTypeChangeable = new BehaviorSubject<ProductType>(
      ProductType.marketplace
    );
    this.permissions$ = this.permissions.permissions$
      .pipe(
        distinctUntilChanged()
      );
    this.priceTypeChangeable$ = this.priceTypeChangeable.asObservable();

    this.priceType$ = this.priceType()
      .pipe(
        distinctUntilChanged(),
        shareReplay({ refCount: true }
        ));

    this.visibleToggle$ = this.getMixedPermissions()
      .pipe(distinctUntilChanged(),
        shareReplay({ refCount: true }
        ));

    this.showPromoPrices$ = this.priceType$.pipe(
      map(type => type === ProductType.marketplace)
    );
  }

  public setPriceType(priceType: ProductType): void {
    this.priceTypeChangeable.next(priceType);
  }

  private hasReadPermissions(permissions: NgxPermissionsObject): boolean {
    const res1 = this.hasPermission(permissions, Permission.ReadProductsMarketplace);
    const res2 = this.hasPermission(permissions, Permission.ReadProductsVendorCatalog);
    return res1 && res2;
  }

  private hasMarketplacePermissions(permissions: NgxPermissionsObject): boolean {
    const res1 = this.hasPermission(permissions, Permission.ReadProductsMarketplace);
    const res2 = this.hasPermission(permissions, Permission.EditProductsMarketplace);
    return res1 || res2;
  }

  private hasVendorCatalogPermissions(permissions: NgxPermissionsObject): boolean {
    const res1 = this.hasPermission(permissions, Permission.ReadProductsVendorCatalog);
    const res2 = this.hasPermission(permissions, Permission.EditProductsVendorCatalog);
    return res1 || res2;
  }

  private hasPermission(permissions: NgxPermissionsObject, permission: string): boolean {
    if (permissions[permission as any]) {
      return true;
    }
    return false;
  }

  private getMixedPermissions(): Observable<boolean> {
    return this.permissions$.pipe(map(this.hasReadPermissions.bind(this)));
  }

  private priceType(): Observable<ProductType> {
    const mixed$ = this.getMixedPermissions();
    const marketplace$ = this.permissions$.pipe(map(this.hasMarketplacePermissions.bind(this)));
    const vendor$ = this.permissions$.pipe(map(this.hasVendorCatalogPermissions.bind(this)));

    return combineLatest([
      mixed$,
      marketplace$,
      vendor$
    ]).pipe(
      mergeMap(([
        mixed,
        marketplace,
        vendor
      ]) => {
        if (mixed) {
          return this.priceTypeChangeable$;
        }

        if (marketplace) {
          return of(ProductType.marketplace);
        }

        if (vendor) {
          return of(ProductType.vendorCatalog);
        }

        return of(ProductType.marketplace);
      })
    );
  }
}
