/* eslint-disable no-unused-vars */

import { HttpEvent } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

import {
  PriceApprove,
  PriceResponse,
  PriceUpdate
} from '../../../products/interfaces/price.interface';
import { PriceController } from './price-controller.model';


@Injectable({
  providedIn: 'root'
})

export class MockPriceController implements PriceController {

  postApprove(uid: string): Observable<PriceResponse> {
    return of(
      {requestId: '33cff254-c798-42af-af58-a4c0a6ce0088',
        data: {success: true}
      }
    );
  }

  postApproveArray(array: PriceApprove[]): Observable<PriceResponse> {
    return of();
  }

  postDisapprove(uid: string): Observable<PriceResponse> {
    return of();
  }

  postFile(file: FormData): Observable<HttpEvent<any>> {
    return of();
  }

  postPrices(prices: PriceUpdate[]): Observable<PriceResponse> {
    return of(
      {requestId: '33cff254-c798-42af-af58-a4c0a6ce0088',
        data: {success: true}
      }
    );
  }
}
