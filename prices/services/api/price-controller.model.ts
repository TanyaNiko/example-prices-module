import { HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';

import {
  PriceApprove,
  PriceResponse,
  PriceUpdate
} from '../../../products/interfaces/price.interface';

export interface PriceController {
  postPrices(prices: PriceUpdate[]): Observable<PriceResponse>;
  postApprove(uid: string): Observable<PriceResponse>;
  postApproveArray(array: PriceApprove[]): Observable<PriceResponse>;
  postDisapprove(uid: string): Observable<PriceResponse>;

  postFile(file: FormData): Observable<HttpEvent<any>>;
}
