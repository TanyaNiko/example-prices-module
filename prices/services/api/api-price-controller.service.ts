import { HttpEvent } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { ApiWrapperService } from '../../../../core/services/api/api-wrapper.service';
import {
  PriceApprove,
  PriceResponse,
  PriceUpdate
} from '../../../products/interfaces/price.interface';
import { PriceController } from './price-controller.model';

@Injectable({
  providedIn: 'root'
})

export class ApiPriceController implements PriceController {
  constructor(private api: ApiWrapperService) { }

  postApprove(uid: string): Observable<PriceResponse> {
    return this.api.post(`/prices/approve`, uid);
  }

  postApproveArray(array: PriceApprove[]): Observable<PriceResponse> {
    return this.api.post(`/prices/approve`, array);
  }

  postDisapprove(uid: string): Observable<PriceResponse> {
    return this.api.post(`/prices/disapprove`, uid);
  }

  postFile(file: FormData): Observable<HttpEvent<any>> {
    return this.api.postFormData('/prices/file', file);
  }

  postPrices(prices: PriceUpdate[]): Observable<any> {
    return this.api.post(`/prices`, prices);
  }
}
