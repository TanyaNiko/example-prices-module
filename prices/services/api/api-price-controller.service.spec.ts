import { TestBed } from '@angular/core/testing';

import { ApiWrapperService } from '../../../../core/services/api/api-wrapper.service';
import { ApiPriceController } from './api-price-controller.service';


describe('ApiPriceControllerService', () => {
  let service: ApiPriceController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        {
          provide: ApiWrapperService,
          useValue: {
            currentUser: jasmine.createSpy('currentUser$').and.callThrough()
          }
        }
      ]
    });
    service = TestBed.inject(ApiPriceController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
