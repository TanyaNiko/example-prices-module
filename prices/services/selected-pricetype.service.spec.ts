import { inject, TestBed } from '@angular/core/testing';
import { NgxPermissionsModule } from 'ngx-permissions';

import { SelectedPriceTypeService } from './selected-price-type.service';

describe('Service: QuotasSelectedWarehouse', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [NgxPermissionsModule.forRoot()],
      providers: [SelectedPriceTypeService]
    });
  });

  it('should ...', inject([SelectedPriceTypeService],
    (service: SelectedPriceTypeService) => {
      expect(service).toBeTruthy();
    }));
});
