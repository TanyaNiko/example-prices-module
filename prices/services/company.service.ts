import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import {
  CompanyCategory,
  CompanyContact,
  CompanyDTO,
  CompanyPriceFilter
} from '../../../core/interfaces/company.interface';
import { ApiCompanyManagement } from '../../../core/services/api/company-management/api-company-management.service';

@Injectable({
  providedIn: 'root'
})
export class CompanyService {
  constructor(
    private apiCompanyManagementService: ApiCompanyManagement
  ) {}

  public getCompanies(): Observable<CompanyPriceFilter[]> {
    return this.apiCompanyManagementService.getCompanies().pipe(
      map((res) => {
        if (!res?.data) {
          return this.emptyResult();
        }

        return res.data.map((company) =>
          this.mapCompany(company)
        ) as CompanyPriceFilter[];
      })
    );
  }

  mapCompany(item: CompanyDTO): CompanyPriceFilter {
    return {
      supplierCode: item.supplierCode || '',
      brands: (item.brands ?? []).map(
        (brand) => brand as unknown as CompanyCategory
      ),
      categories: (item.categories ?? []).map(
        (category) => category as unknown as CompanyCategory
      ),
      companyCharter: item.companyCharter as CompanyContact,
      fullName: item.fullName,
      name: item.name
    };
  }

  emptyResult() {
    return [] as CompanyPriceFilter[];
  }
}
