import { Observable } from 'rxjs';

export abstract class ItemsService<T> {
  public abstract get(
    filters: T,
    params: { limit: number; offset: number; sortBy: string; sortOrder: 'asc' | 'desc' }
  ): Observable<{ data: T[]; totalCount: number }>;
}
