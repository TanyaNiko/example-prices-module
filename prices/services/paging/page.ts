import { Observable } from 'rxjs';

export interface ISort<T> {
  property: keyof T;
  order: 'asc' | 'desc';
}

export interface IPageRequest<T> {
  page: number;
  size: number;
  sort?: ISort<T>;
}

export interface IPage<T> {
  content: T[];
  totalElements: number;
  size: number;
  pageNumber: number;
}

export type PaginatedEndpoint<T, Q>
  = (pageable: IPageRequest<T>, query: Q) => Observable<IPage<T>>;
