import { ISort } from './page';
import { PaginatedDataSource } from './paginated-datasource';
import { User } from './user';
import { TestDataService, UserQuery } from './user.service';

export class UsersComponent {
  displayedColumns = [
    'id',
    'username',
    'email',
    'registration'
  ];

  initialSort: ISort<User> = { property: 'username',
    order: 'asc' };

  data = new PaginatedDataSource<User, UserQuery>(
    (request, query) => this.users.page(request, query),
    this.initialSort,
    { search: '',
      registration: undefined },
    2
  );

  constructor(private users: TestDataService) {

  }
}
