import { DataSource } from '@angular/cdk/collections';
import { BehaviorSubject, combineLatest, Observable, Subject } from 'rxjs';
import { map, share, startWith, switchMap } from 'rxjs/operators';

import { indicate } from './operators';
import { IPage, ISort, PaginatedEndpoint } from './page';

export interface SimpleDataSource<T> extends DataSource<T> {
  connect(): Observable<T[]>;
  disconnect(): void;
}


export class PaginatedDataSource<T, Q> implements SimpleDataSource<T> {

  loading$: Observable<boolean>;

  public page$: Observable<IPage<T>>;

  private pageNumber = new Subject<number>();

  private sort: BehaviorSubject<ISort<T>>;

  private query: BehaviorSubject<Q>;

  private loading = new Subject<boolean>();

  constructor(
    private endpoint: PaginatedEndpoint<T, Q>,
    initialSort: ISort<T>,
    initialQuery: Q,
    public pageSize = 20) {
    this.loading$ = this.loading.asObservable();
    this.query = new BehaviorSubject<Q>(initialQuery);
    this.sort = new BehaviorSubject<ISort<T>>(initialSort);
    const param$ = combineLatest([
      this.query,
      this.sort
    ]);
    this.page$ = param$.pipe(
      switchMap(([
        query,
        sort
      ]) => this.pageNumber.pipe(
        startWith(0),
        switchMap(page => this.endpoint({ page,
          sort,
          size: this.pageSize }, query)
          .pipe(indicate(this.loading))
        )
      )),
      share()
    );
  }

  sortBy(sort: Partial<ISort<T>>): void {
    const lastSort = this.sort.getValue();
    const nextSort = { ...lastSort,
      ...sort };
    this.sort.next(nextSort);
  }

  queryBy(query: Partial<Q>): void {
    const lastQuery = this.query.getValue();
    const nextQuery = { ...lastQuery,
      ...query };
    this.query.next(nextQuery);
  }

  fetch(page: number): void {
    this.pageNumber.next(page);
  }

  connect(): Observable<T[]> {
    return this.page$.pipe(map(page => page.content));
  }

  disconnect(): void { }

}
