/* eslint-disable @typescript-eslint/no-shadow */
/* eslint-disable no-shadow */
/* tslint:disable:no-shadowed-variable*/
import { DataSource } from '@angular/cdk/collections';
import { BehaviorSubject, combineLatest, Observable, Subject } from 'rxjs';
import { pluck, share, startWith, switchMap } from 'rxjs/operators';

import { indicate } from './operators';
import { IPage, ISort, PaginatedEndpoint } from './page';

export interface SimpleDataSource<T> extends DataSource<T> {
  connect(): Observable<T[]>;
  disconnect(): void;
}

export interface PaginatedDataSource<T, Q> extends SimpleDataSource<T> {
  sortBy: (s: ISort<T>) => void;
  queryBy: (q: Partial<Q>) => void;
  fetch: (p: number) => void;
  page$: Observable<IPage<T>>;
  loading$: Observable<boolean>;
}

export function paginatedDataSource<T, Q>(
  endpoint: PaginatedEndpoint<T, Q>,
  initialSort: ISort<T>,
  initialQuery: Q,
  pageSize = 20
): PaginatedDataSource<T, Q> {
  const pageNumber = new Subject<number>();
  const loading = new Subject<boolean>();

  const sort = new BehaviorSubject<ISort<T>>(initialSort);
  const query = new BehaviorSubject<Q>(initialQuery);
  const param$ = combineLatest([
    query,
    sort
  ]);
  const page$ = param$.pipe(
    switchMap(([
      query,
      sort
    ]) => pageNumber.pipe(
      startWith(0),
      switchMap(page => endpoint({ page,
        sort,
        size: pageSize }, query)
        .pipe(indicate(loading))
      )
    )
    ),
    share()
  );
  const queryBy = (q: Partial<Q>) => {
    const lastQuery = query.getValue();
    const nextQuery = { ...lastQuery,
      ...q };
    query.next(nextQuery);
  };
  const fetch = (p: number) => pageNumber.next(p);
  const sortBy = (s: ISort<T>) => sort.next(s);
  return {
    sortBy,
    queryBy,
    fetch,
    page$,
    connect: () => page$.pipe(pluck('content')),
    disconnect: () => undefined,
    loading$: loading.asObservable()
  };
}
