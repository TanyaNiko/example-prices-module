import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { delay } from 'rxjs/operators';

import { IPage, IPageRequest } from './page';
import { User } from './user';

export interface UserQuery {
  search: string;
  registration?: Date;
}

@Injectable({ providedIn: 'root' })
export class TestDataService {

  private users: User[] = [
    {
      id: 1,
      name: 'Leanne Graham',
      registrationDate: new Date(1565575044121),
      username: 'Bret',
      email: 'Sincere@april.biz',
      address: {
        street: 'Kulas Light',
        suite: 'Apt. 556',
        city: 'Gwenborough',
        zipcode: '92998-3874',
        geo: {
          lat: '-37.3159',
          lng: '81.1496'
        }
      },
      phone: '1-770-736-8031 x56442',
      website: 'hildegard.org',
      company: {
        name: 'Romaguera-Crona',
        catchPhrase: 'Multi-layered client-server neural-net',
        bs: 'harness real-time e-markets'
      }
    },
    {
      id: 2,
      name: 'Ervin Howell',
      registrationDate: new Date(1565575044121),
      username: 'Antonette',
      email: 'Shanna@melissa.tv',
      address: {
        street: 'Victor Plains',
        suite: 'Suite 879',
        city: 'Wisokyburgh',
        zipcode: '90566-7771',
        geo: {
          lat: '-43.9509',
          lng: '-34.4618'
        }
      },
      phone: '010-692-6593 x09125',
      website: 'anastasia.net',
      company: {
        name: 'Deckow-Crist',
        catchPhrase: 'Proactive didactic contingency',
        bs: 'synergize scalable supply-chains'
      }
    }
  ];

  page(request: IPageRequest<User>, query: UserQuery): Observable<IPage<User>> {
    let filteredUsers = this.users;
    let { search, registration } = query;
    if (search) {
      search = search.toLowerCase();
      filteredUsers = filteredUsers.filter(
        ({ name, username, email }) =>
          name.toLowerCase().includes(search) ||
          username.toLowerCase().includes(search) ||
          email.toLowerCase().includes(search)
      );
    }
    if (registration) {
      filteredUsers = filteredUsers.filter(
        ({ registrationDate }) =>
          registrationDate.getFullYear() === registration?.getFullYear() &&
          registrationDate.getMonth() === registration.getMonth() &&
          registrationDate.getDate() === registration.getDate()
      );
    }
    filteredUsers = [...filteredUsers].sort((a, b) => {
      const propA = a[request?.sort?.property ?? 'id'];
      const propB = b[request?.sort?.property ?? 'id'];
      let result;
      if (typeof propA === 'string') {
        result = propA.toLowerCase().localeCompare(propB.toString().toLowerCase());
      } else {
        result = propA as any - (propB as any);
      }
      const factor = request?.sort?.order === 'asc' ? 1 : -1;
      return result * factor;
    });
    const start = request.page * request.size;
    const end = start + request.size;
    const pageUsers = filteredUsers.slice(start, end);
    const page: IPage<User> = {
      content: pageUsers,
      pageNumber: request.page,
      size: pageUsers.length,
      totalElements: filteredUsers.length
    };
    return of(page).pipe(delay(500));
  }


}
