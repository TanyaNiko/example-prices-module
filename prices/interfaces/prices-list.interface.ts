export interface IPrice {
  productSap: string;
  materialSupplierCode: string;
  manufacturingCode: string;
  productName: string;
  price: number;
  priceStatus: boolean;
  priceERPStatus: boolean;
  dateOut: Date;
  retailerNameAndStatus: IRetailerNameAndStatus;
}

export interface IPriceVm {
  priceNew: number;
  dateOutNew: Date;
  dateInNew: Date;
}

interface IRetailerNameAndStatus {
  mVideoIsActive: boolean;
  mVideoIsInactive: boolean;
  eldoradoIsActive: boolean;
  eldoradoIsInactive: boolean;
}
