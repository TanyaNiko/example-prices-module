import { NgModule } from '@angular/core';
import { MatTooltipModule } from '@angular/material/tooltip';
import { NgxMaskModule } from 'ngx-mask';

import { CurrencyModule } from '../../shared/modules/currency/currency.module';
import { SharedModule } from '../../shared/shared.module';
import { RegistrationService } from '../registration-org/services/registration.service';
import { FiltersComponent } from './components/filters/filters.component';
import { PriceApproveDialogComponent } from './components/price-aprove-dialog/price-approve-dialog.component';
import { PriceStatusImgComponent } from './components/price-status-img/price-status-img.component';
import { PricesTableComponent } from './components/prices-table/prices-table.component';
import { PricesComponent } from './components/routs/prices/prices.component';
import { PricesEditComponent } from './components/routs/prices-edit/prices-edit.component';
import { PricesListComponent } from './components/routs/prices-list/prices-list.component';
import { PricesMatchingComponent } from './components/routs/prices-matching/prices-matching.component';
import { SelectCompanyComponent } from './components/select-company/select-company.component';
import { TooltipPriceComponent } from './components/tooltip-price/tooltip-price.component';
import { PriceIsPositivePipe } from './pipes/price-is-positive.pipe';
import { PricePercentageChangesPipe } from './pipes/price-percentage-changes.pipe';
import { PriceStatusNamePipe } from './pipes/price-status-name.pipe';
import { TooltipStatusNamePipe } from './pipes/tooltip-status-name.pipe';
import { PricesRoutingModule } from './prices-routing.module';

@NgModule({
  imports: [
    SharedModule,
    PricesRoutingModule,
    NgxMaskModule,
    MatTooltipModule,
    CurrencyModule
  ],
  declarations: [
    PricesComponent,
    FiltersComponent,
    SelectCompanyComponent,
    PricesEditComponent,
    PricesMatchingComponent,
    PricesListComponent,
    PricesTableComponent,
    TooltipPriceComponent,
    PriceStatusImgComponent,
    TooltipStatusNamePipe,
    PriceStatusNamePipe,
    PriceApproveDialogComponent,
    PricePercentageChangesPipe,
    PriceIsPositivePipe
  ],
  providers: [RegistrationService]
})
export class PricesModule { }
