import { CollectionViewer } from '@angular/cdk/collections';
import { DataSource } from '@angular/cdk/table';
import { PageEvent } from '@angular/material/paginator';
import { Sort } from '@angular/material/sort';
import {
  BehaviorSubject,
  combineLatest,
  ReplaySubject,
  Subject
} from 'rxjs';
import { Observable } from 'rxjs';
import {
  debounceTime,
  pluck,
  repeat,
  shareReplay,
  startWith,
  switchMap,
  takeUntil,
  tap
} from 'rxjs/operators';

import { Product } from '../../products/interfaces/product.interface';
import { IPrice, IPriceVm } from '../interfaces/prices-list.interface';
import { PriceService } from '../services/price.service';

export class PricesDataSource implements DataSource<Product> {
  public readonly prices$: Observable<Product[]>;

  public readonly length$: Observable<number>;

  public readonly params$: ReplaySubject<{
    offset: number;
    limit: number;
    supplierCode?: string;
  }>;

  public readonly sort$: ReplaySubject<{ sortBy: string; sortOrder: string }>;

  public readonly filter$: ReplaySubject<any>;

  public readonly loading$: ReplaySubject<boolean>;

  private pricesSubject = new BehaviorSubject<Product[]>([]);

  private loadingSubject = new BehaviorSubject<boolean>(false);

  private readonly destroy$: Subject<void>;

  private readonly reload$: Subject<void>;

  constructor(
    private priceService: PriceService,
    private supplierCode: string = ''
  ) {
    this.destroy$ = new Subject();

    this.params$ = new ReplaySubject();
    this.loading$ = new ReplaySubject();
    this.sort$ = new ReplaySubject();
    this.filter$ = new ReplaySubject();
    this.reload$ = new Subject();

    const prices = this.initPrices().pipe(takeUntil(this.destroy$));

    this.prices$ = prices.pipe(pluck('data'));
    this.length$ = prices.pipe(pluck('totalCount'));
  }

  connect(collectionViewer: CollectionViewer): Observable<Product[]> {
    return this.pricesSubject.asObservable();
  }

  disconnect(collectionViewer?: CollectionViewer): void {
    this.pricesSubject.complete();
    this.loadingSubject.complete();

    this.params$.complete();
    this.loading$.complete();
    this.sort$.complete();

    this.destroy$.next();
    this.destroy$.complete();
  }

  loadPrices(filter = '', sortDirection = 'asc',
    pageIndex = 0, pageSize = 10) {

    this.loadingSubject.next(true);

    this.priceService.get({} as IPrice, { offset: 0, limit: 10 } as any)
      .pipe(
        pluck('data'),
        catchError(() => of([])),
        finalize(() => this.loadingSubject.next(false))
      )
      .subscribe(prices => this.pricesSubject.next(prices));
  }

  initPrices() {
    return combineLatest([
      this.filter$.pipe(startWith({})),
      this.params$.pipe(
        startWith({ offset: 0,
          limit: 10,
          supplierCode: this.supplierCode })
      ),
      this.sort$.pipe(startWith({}))
    ]).pipe(
      debounceTime(500),
      switchMap(([
        filters,
        params,
        sort
      ]) => {
        this.loading$.next(true);
        return this.priceService
          .get(filters, { ...params,
            ...sort } as any)
          .pipe(
            takeUntil(this.destroy$),
            tap(() => this.loading$.next(false))
          );
      }),
      takeUntil(this.reload$),
      repeat(),
      shareReplay()
    );
  }

  reload(): void {
    this.reload$.next();
  }

  setPage(page: PageEvent) {
    this.params$.next({
      offset: page.pageIndex * page.pageSize,
      limit: page.pageSize,
      supplierCode: this.supplierCode ?? ''
    });
  }

  setSort(sort: Sort) {
    this.sort$.next({
      sortBy: sort.active,
      sortOrder: sort.direction
    });
  }

  setNewPrice(newValue: any, item: IPrice & IPriceVm) {
    if (!newValue) {
      return;
    }
    item.priceNew = newValue;
    this.priceService.setNewPriceAndDate();
  }

  setNewDate(newValue: any, item: IPrice & IPriceVm) {
    if (!newValue?.dateTo) {
      return;
    }
    item.dateOutNew = newValue.dateTo;
    this.priceService.setNewPriceAndDate();
  }
}
